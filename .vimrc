
" turn hybrid line numbers on
set number relativenumber
syntax on
set listchars=space:·,tab:»»
set list
set tabstop=4 shiftwidth=4 expandtab
set laststatus=2
set cursorline
colorscheme Qiita-theme " look at my vim-qiita-theme repo on Gitlab

