#!/usr/bin/fish

# Simple fish config
# *in work*

# alias to work with git bare repo to store my dotfiles
set fish_greeting
set PATH $PATH /usr/bin /usr/sbin


#=======#
#ALIASES#
#=======#

# bare git repo alias for dotfiles
alias config='/usr/bin/git --git-dir=/home/wizard/.dots/ --work-tree=/home/wizard'
# to fast work with herbstluftwm/client
alias hc=herbstclient
# modern ls alias
alias ls='exa -al --color=always --group-directories-first'
# Quick shutdown alias
alias Shutdown='shutdown -h now'
# Vim
alias vim='gvim'


#===============================#
#AUTOCOMPLETE & HIGHLIGHT COLORS#
#===============================#

set fish_color_normal         white #'#e3e3e3'
set fish_color_command        green #'#55C500'
set fish_color_quote          red #'#ebd247'
set fish_color_redirection    yellow #'#41b7d7'
set fish_color_end            red
set fish_color_error          '#df4f4f'
set fish_color_param          cyan #'#0f9f7f'
set fish_color_comment        '#9dabae' #'#41b7d7'
set fish_color_match          blue #'#41b7d7'
set fish_color_selection      white #'#e3e3e3'
set fish_color_search_match   blue #'#41b7d7'
set fish_color_operator       yellow #'#bf5fbf'
set fish_color_escape         red #'#dfbf4f'
set fish_color_cwd            green #'#6f5fef'
set fish_color_autosuggestion '#9dabae' #'#576f75'
set fish_color_user           green #'#ae81ff'
set fish_color_host           green #'#bf5fbf'
set fish_color_cancel         red #'#ff8095'


#======#
#PROMPT#
#======#

starship init fish | source

#=========#
#FUNCTIONS#
#=========#

function backup --argument filename
    cp $filename $filename.bak
end

