#!/usr/bin/fish

# this is my custom fish config for herbstluftwm

function hc() # FIXME
  herbstclient "$@"
end

hc emit_hook reload

# # # # #
# THEME #
# # # # #

# TODO: Set color theme

xsetroot -solid '#2A4B52'

hc attr theme.tiling.reset 1
hc attr theme.floating.reset 1
hc set frame_border_active_color '#55C500'
hc set frame_border_normal_color '#010101'
hc set frame_bg_normal_color '#565656'
hc set frame_bg_active_color '#345F0C'
hc set frame_border_width 2
hc set always_show_frame 1
hc set frame_bg_transparent 1
hc set frame_transparent_width 1
hc set frame_gap 4

hc attr theme.active.color '#55C500'
hc attr theme.normal.color '#4C6C7C'
hc attr theme.urgent.color orange
hc attr theme.inner_width 1
hc attr theme.inner_color black
hc attr theme.border_width 3
hc attr theme.floating.border_width 4
hc attr theme.floating.outer_width 1
hc attr theme.floating.outer_color black
hc attr theme.active.inner_color '#3E4A00'
hc attr theme.active.outer_color '#3E4A00'
hc attr theme.background_color '#141414'

hc set window_gap 0
hc set frame_padding 0
hc set smart_window_surroundings 0
hc set smart_frame_surroundings 1
hc set mouse_recenter_gap 0

# # # # # # # #
# KEYBINDINGS #
# # # # # # # #

# TODO:
# $Win-$Run-d|t|f|.. section
# Meaning: Open new frame and run app in it
# TODO:
# break function : Win-b
# Meaning: breaks current frame and closes all contained windows

hc keyunbind --all # removes all existing keybindings

Run=Mod1
Win=Mod4
Move=Shift
Change=Control

# Quick apps. Tip: spawns apps in current frame
hc keybind $Run-d spawn dmenu_run_hlwm
hc keybind $Run-f spawn firefox
hc keybind $Run-t spawn alacritty
hc keybind $Run-r reload # executes autostart file

# Window managing
hc keybind $Win-c close # closes window itself, leaves frame
hc keybind $Win-r remove # removes frame itself, merges windows with neighbor
hc keybind $Win-s floating toggle # floating mod
hc keybind $Win-f fullscreen toggle # fullscreen mod
hc keybind $Win-p pseudotile toggle # pseudotile mod

# Frame managing
hc keybind $Win-space-h split   left 0.5
hc keybind $Win-space-j split   bottom 0.5
hc keybind $Win-space-k split   top 0.5
hc keybind $Win-space-l split   right 0.5
hc keybind $Win-space-Left  split   left 0.5
hc keybind $Win-space-Down  split   bottom 0.5
hc keybind $Win-space-Up    split   top 0.5
hc keybind $Win-space-Right split   right 0.5
hc keybind $Win-space-e split explode # explodes current frame into two subframes

# Focus changes
hc keybind $Win-Left  focus left
hc keybind $Win-Down  focus down
hc keybind $Win-Up    focus up
hc keybind $Win-Right focus right
hc keybind $Win-h     focus left
hc keybind $Win-j     focus down
hc keybind $Win-k     focus up
hc keybind $Win-l     focus right

# Move actions
hc keybind $Win-$Move-Left  shift left
hc keybind $Win-$Move-Down  shift down
hc keybind $Win-$Move-Up    shift up
hc keybind $Win-$Move-Right shift right
hc keybind $Win-$Move-h     shift left
hc keybind $Win-$Move-j     shift down
hc keybind $Win-$Move-k     shift up
hc keybind $Win-$Move-l     shift right

# Resize frame
set resizestep=0.025
hc keybind $Win-$Change-h       resize left +$resizestep
hc keybind $Win-$Change-j       resize down +$resizestep
hc keybind $Win-$Change-k       resize up +$resizestep
hc keybind $Win-$Change-l       resize right +$resizestep
hc keybind $Win-$Change-Left    resize left +$resizestep
hc keybind $Win-$Change-Down    resize down +$resizestep
hc keybind $Win-$Change-Up      resize up +$resizestep
hc keybind $Win-$Change-Right   resize right +$resizestep

# # # # # # # # #
# TAGS/MONITORS #
# # # # # # # # #

# TODO: Check $Mod

# sets monitors
hc set_monitors 1920x1200+0+0
xrandr -s 1920x1200

# tags
tag_names=( {1..9} )
tag_keys=( {1..9} 0 )

hc rename default "${tag_names[0]}" || true
for i in ${!tag_names[@]} ; do
    hc add "${tag_names[$i]}"
    key="${tag_keys[$i]}"
    if ! [ -z "$key" ] ; then
        hc keybind "$Mod-$key" use_index "$i" #FIXME
        hc keybind "$Mod-Shift-$key" move_index "$i" #FIXME
    end 
end

# cycle through tags # FIXME: it should be in kebs, shouldnt it?
hc keybind $Win-period use_index +1 --skip-visible
hc keybind $Win-comma  use_index -1 --skip-visible

# TODO: WHAT?
# The following cycles through the available layouts within a frame, but skips
# layouts, if the layout change wouldn't affect the actual window positions.
# I.e. if there are two windows within a frame, the grid layout is skipped.
hc keybind $Win-space                                                           \
            or , and . compare tags.focus.curframe_wcount = 2                   \
                     . cycle_layout +1 vertical horizontal max vertical grid    \
               , cycle_layout +1

# mouse
hc mouseunbind --all
hc mousebind $Win-Button1 move
hc mousebind $Win-Button2 zoom
hc mousebind $Win-Button3 resize

# focus FIXME to keybs?
hc keybind $Win-BackSpace   cycle_monitor
hc keybind $Win-Tab         cycle_all +1
hc keybind $Win-Shift-Tab   cycle_all -1
hc keybind $Win-c cycle
hc keybind $Win-i jumpto urgent

# # # # # # #
# BEHAVIOR  #
# # # # # # #

# TODO: CHECK
# rules
hc unrule -F

hc rule focus=on # normally focus new clients
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(DIALOG|UTILITY|SPLASH)' pseudotile=on
hc rule windowtype='_NET_WM_WINDOW_TYPE_DIALOG' focus=on
hc rule windowtype~'_NET_WM_WINDOW_TYPE_(NOTIFICATION|DOCK|DESKTOP)' manage=off

hc set tree_style '╾│ ├└╼─┐'

# unlock, just to be sure
hc unlock

# # # # #
# PANEL #
# # # # #

# find the panel
panel=~/.config/herbstluftwm/panel.sh
[ -x "$panel" ] || panel=/etc/xdg/herbstluftwm/panel.sh
for monitor in $(herbstclient list_monitors | cut -d: -f1) ; do
    # start it on each monitor
    "$panel" $monitor &
end

# # # #
# END #
# # # #
